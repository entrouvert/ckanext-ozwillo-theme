# Bulgarian translations for ckanext-ozwillo-theme.
# Copyright (C) 2015 ORGANIZATION
# This file is distributed under the same license as the
# ckanext-ozwillo-theme project.
# FIRST AUTHOR <EMAIL@ADDRESS>, 2015.
#
msgid ""
msgstr ""
"Project-Id-Version: ckanext-ozwillo-theme 0.0.1\n"
"Report-Msgid-Bugs-To: EMAIL@ADDRESS\n"
"POT-Creation-Date: 2015-05-29 17:14+0200\n"
"PO-Revision-Date: 2015-02-03 14:30+0100\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: bg <LL@li.org>\n"
"Plural-Forms: nplurals=2; plural=(n != 1)\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=utf-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Generated-By: Babel 0.9.6\n"

#: ckanext/ozwillo_theme/templates/footer.html:9
msgid "Common goods factory"
msgstr "Създател на общи ползи"

#: ckanext/ozwillo_theme/templates/footer.html:20
#: ckanext/ozwillo_theme/templates/header.html:35
#: ckanext/ozwillo_theme/templates/header.html:88
msgid "News"
msgstr "Новини"

#: ckanext/ozwillo_theme/templates/footer.html:21
msgid "Discovering"
msgstr "Изучаване"

#: ckanext/ozwillo_theme/templates/footer.html:22
#: ckanext/ozwillo_theme/templates/header.html:89
msgid "Co-construct"
msgstr "Съвместно изграждане "

#: ckanext/ozwillo_theme/templates/footer.html:23
#: ckanext/ozwillo_theme/templates/header.html:90
msgid "Let's go"
msgstr "Стартиране "

#: ckanext/ozwillo_theme/templates/footer.html:27
msgid "Contact"
msgstr "Връзки"

#: ckanext/ozwillo_theme/templates/footer.html:28
msgid "Project team"
msgstr "Екипът на проекта "

#: ckanext/ozwillo_theme/templates/footer.html:29
#: ckanext/ozwillo_theme/templates/header.html:91
msgid "Projects"
msgstr "Проекти"

#: ckanext/ozwillo_theme/templates/footer.html:35
msgid "User guide"
msgstr "Ръководство на потребителя"

#: ckanext/ozwillo_theme/templates/footer.html:36
msgid "Developers"
msgstr "Pазработчиците"

#: ckanext/ozwillo_theme/templates/footer.html:37
msgid "Legal Notices"
msgstr "Правни бележки"

#: ckanext/ozwillo_theme/templates/footer.html:38
msgid "General terms of use"
msgstr "Условия за ползване "

#: ckanext/ozwillo_theme/templates/header.html:11
#: ckanext/ozwillo_theme/templates/header.html:52
#: ckanext/ozwillo_theme/templates/header.html:61
msgid "Home"
msgstr ""

#: ckanext/ozwillo_theme/templates/header.html:15
#: ckanext/ozwillo_theme/templates/header.html:55
msgid "Catalog"
msgstr "Каталог"

#: ckanext/ozwillo_theme/templates/header.html:19
#: ckanext/ozwillo_theme/templates/header.html:27
#: ckanext/ozwillo_theme/templates/header.html:59
#: ckanext/ozwillo_theme/templates/header.html:69
msgid "Data"
msgstr "данни"

#: ckanext/ozwillo_theme/templates/header.html:21
#: ckanext/ozwillo_theme/templates/header.html:62
#: ckanext/ozwillo_theme/templates/user/read_base.html:19
#: ckanext/ozwillo_theme/templates/user/read_base.html:53
msgid "Datasets"
msgstr ""

#: ckanext/ozwillo_theme/templates/header.html:22
#: ckanext/ozwillo_theme/templates/header.html:63
msgid "Organizations"
msgstr ""

#: ckanext/ozwillo_theme/templates/header.html:23
#: ckanext/ozwillo_theme/templates/header.html:65
msgid "Dashboard"
msgstr ""

#: ckanext/ozwillo_theme/templates/header.html:32
#: ckanext/ozwillo_theme/templates/header.html:85
msgid "Discover"
msgstr "Изучаване"

#: ckanext/ozwillo_theme/templates/header.html:39
#: ckanext/ozwillo_theme/templates/header.html:80
msgid "Log out"
msgstr ""

#: ckanext/ozwillo_theme/templates/header.html:41
#: ckanext/ozwillo_theme/templates/header.html:82
msgid "Log in"
msgstr "Вход"

#: ckanext/ozwillo_theme/templates/header.html:87
msgid "Discovering the Platform"
msgstr "Изучаване на платформата"

#: ckanext/ozwillo_theme/templates/page.html:6
msgid "Skip to content"
msgstr ""

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:5
msgid "Open Data"
msgstr "Отворени данни"

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:7
msgid "Searching for data?"
msgstr "Търсите данни?"

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:10
msgid "Explore"
msgstr "Изследвайте"

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:10
msgid "datasets published on Ozwillo by local authorities and public bodies."
msgstr ""
"данните, публикувани на Ozwillo от местните органи на властта и публични "
"организации."

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:13
msgid "You have data to share?"
msgstr "Имате данни за споделяне?"

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:16
msgid "If you search a place to host them, subscribe to our service"
msgstr ""
"Ако търсите място, на което да ги съхранявате, запишете се за нашата "
"услуга"

#: ckanext/ozwillo_theme/templates/home/snippets/promoted.html:16
msgid ": click Install and steer by it. An Ozwillo account is required."
msgstr ""
": напитснете Инсталация и следвайте инструкциите. Изисква се регистрация "
"в Ozwillo."

#: ckanext/ozwillo_theme/templates/home/snippets/search.html:2
msgid "eg. Gold Prices"
msgstr ""

#: ckanext/ozwillo_theme/templates/home/snippets/search.html:7
msgid "Search Your Data"
msgstr ""

#: ckanext/ozwillo_theme/templates/home/snippets/search.html:10
msgid "Search"
msgstr ""

#: ckanext/ozwillo_theme/templates/home/snippets/search.html:15
msgid "Popular Tags"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:5
#: ckanext/ozwillo_theme/templates/user/read_base.html:8
msgid "Users"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:14
msgid "Manage"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:20
msgid "Activity Stream"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:39
msgid "You have not provided a biography."
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:41
msgid "This user has no biography."
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:49
msgid "Followers"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:57
msgid "Edits"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:73
msgid "Open ID"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:76
msgid "Username"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:82
msgid "Email"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:82
#: ckanext/ozwillo_theme/templates/user/read_base.html:96
msgid "This means only you can see this"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:82
#: ckanext/ozwillo_theme/templates/user/read_base.html:96
msgid "Private"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:87
msgid "Member Since"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:91
msgid "State"
msgstr ""

#: ckanext/ozwillo_theme/templates/user/read_base.html:96
msgid "API Key"
msgstr ""

